package com.payplatter.bean;

public class MerchantServicesListBean {
	private Integer Id;
	private String servicesName;
	
	private Integer productId;
	private String product_name;
	private String subscription_price;
	private String product_description;
	private String product_image;
	private String product_link;
	private String product_config_link;
	private String product_tag_line;
	
	private Integer pgId;
	private Integer mId;
	
	
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public String getServicesName() {
		return servicesName;
	}
	public void setServicesName(String servicesName) {
		this.servicesName = servicesName;
	}
	public Integer getProductId() {
		return productId;
	}
	public void setProductId(Integer productId) {
		this.productId = productId;
	}
	public String getProduct_name() {
		return product_name;
	}
	public void setProduct_name(String product_name) {
		this.product_name = product_name;
	}
	public String getSubscription_price() {
		return subscription_price;
	}
	public void setSubscription_price(String subscription_price) {
		this.subscription_price = subscription_price;
	}
	public String getProduct_description() {
		return product_description;
	}
	public void setProduct_description(String product_description) {
		this.product_description = product_description;
	}
	public String getProduct_image() {
		return product_image;
	}
	public void setProduct_image(String product_image) {
		this.product_image = product_image;
	}
	public String getProduct_link() {
		return product_link;
	}
	public void setProduct_link(String product_link) {
		this.product_link = product_link;
	}
	public String getProduct_config_link() {
		return product_config_link;
	}
	public void setProduct_config_link(String product_config_link) {
		this.product_config_link = product_config_link;
	}
	public String getProduct_tag_line() {
		return product_tag_line;
	}
	public void setProduct_tag_line(String product_tag_line) {
		this.product_tag_line = product_tag_line;
	}
	public Integer getPgId() {
		return pgId;
	}
	public void setPgId(Integer pgId) {
		this.pgId = pgId;
	}
	public Integer getmId() {
		return mId;
	}
	public void setmId(Integer mId) {
		this.mId = mId;
	}
	
	
	

}
