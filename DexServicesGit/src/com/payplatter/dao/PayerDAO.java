package com.payplatter.dao;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.payplatter.bean.MerchantBean;
import com.payplatter.bean.MessageBean;
import com.payplatter.bean.PayerBean;
import com.payplatter.bean.TransactionBean;
import com.sabpaisa.portal.utilityclasses.DBConnection;

public class PayerDAO {
	static Logger log = Logger.getLogger(PayerDAO.class);
	DBConnection conn = new DBConnection();
	Connection con = null;
	Statement stmt = null;

	public PayerBean getPayerProfile(String pid) throws SQLException {
		con = conn.getConnection();
		ResultSet rs = null;
		PayerBean bean = null;
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM payplatter.payer_master where Id='" + pid + "'";
			rs = stmt.executeQuery(sql);

			if (rs != null && rs.next()) {
				bean = new PayerBean();
				bean.setId(Integer.toString(rs.getInt("Id")));
				bean.setFirstName(rs.getString("firstName"));
				bean.setLastName(rs.getString("lastName"));
				bean.setEmailId(rs.getString("emailId"));
				bean.setContact(rs.getString("contact"));
				bean.setCity(rs.getString("City"));
				bean.setState(rs.getString("State"));
				bean.setCountry(rs.getString("Country"));
				bean.setZip(rs.getString("Zip"));
				bean.setAdd1(rs.getString("Add1"));
				bean.setStatus(rs.getString("status"));
				// bean.setCreatedDate(rs.getTimestamp("createdDate"));

			} else {
				bean = new PayerBean();
				bean.setStatus("Record Not Found");
			}
			return bean;

		} catch (Exception e) {
			System.out.println(e);
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}
		return bean;
	}

	public List<TransactionBean> getPayerTransactionMerchantWise(String peid, String mid) throws SQLException {
		con = conn.getConnection();
		ResultSet rs = null;
		List<TransactionBean> list = new ArrayList<>();
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM transactions_details where email='" + peid + "' and merchant_Id_fk='" + mid
					+ "' and payer_type='CONSUMERS'";

			log.info("SQL =" + sql);
			rs = stmt.executeQuery(sql);
			TransactionBean bean = null;
			while (rs.next()) {
				bean = new TransactionBean();
				bean.setId(rs.getInt("id"));
				bean.setName(rs.getString("name"));
				bean.setTransId(rs.getString("transId"));
				bean.setContact(rs.getString("contact"));
				bean.setActAmount(rs.getDouble("actAmount"));
				bean.setTransAmount(rs.getDouble("transAmount"));
				bean.setEmail(rs.getString("email"));
				bean.setSpTransId(rs.getString("spTransId"));
				bean.setTransStatus(rs.getString("transStatus"));
				bean.setTxnCreateDate(rs.getTimestamp("txnCreateDate"));
				bean.setDate((rs.getTimestamp("txnCreateDate")).toString());
				bean.setMerchant_Id_fk(rs.getInt("merchant_Id_fk"));
				list.add(bean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}
		return list;

	}

	public List<TransactionBean> getPayerTransaction(String peid) throws SQLException {
		con = conn.getConnection();
		ResultSet rs = null;
		List<TransactionBean> list = new ArrayList<>();
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM transactions_details where email='" + peid + "' and payer_type='CONSUMERS'";

			log.info("SQL =" + sql);
			rs = stmt.executeQuery(sql);
			TransactionBean bean = null;
			while (rs.next()) {
				bean = new TransactionBean();
				bean.setId(rs.getInt("id"));
				bean.setName(rs.getString("name"));
				bean.setTransId(rs.getString("transId"));
				bean.setContact(rs.getString("contact"));
				bean.setActAmount(rs.getDouble("actAmount"));
				bean.setSpTransId(rs.getString("spTransId"));
				bean.setTransStatus(rs.getString("transStatus"));
				bean.setTxnCreateDate(rs.getTimestamp("txnCreateDate"));
				bean.setMerchant_Id_fk(rs.getInt("merchant_Id_fk"));
				list.add(bean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}
		return list;

	}

	public List<MessageBean> getPayerMessages(String peid, String mid) throws SQLException {
		con = conn.getConnection();
		ResultSet rs = null;
		List<MessageBean> list = new ArrayList<>();
		try {
			stmt = con.createStatement();
			String sql = "SELECT * FROM payplatter.message_config mg,message_master mm where mg.messageIdFk=mm.msgId and mg.payerID="
					+ peid + " and mg.createdBy=" + mid;

			log.info("SQL =" + sql);
			rs = stmt.executeQuery(sql);
			MessageBean bean = null;
			while (rs.next()) {
				bean = new MessageBean();
				bean.setMsgId(rs.getInt("msgId"));
				bean.setCategory(rs.getString("category"));
				bean.setCreatedBy(rs.getInt("createdBy"));
				bean.setCreatedDate(((rs.getTimestamp("createdDate")).toString() == null) ? " " : (rs.getTimestamp("createdDate")).toString());//((rs.getTimestamp("createdDate")).toString() == null) ? " " : rs.getTimestamp("createdDate")).toString())
				bean.setEndDate(((rs.getTimestamp("endDate")).toString() == null) ? " " : (rs.getTimestamp("endDate")).toString());
				bean.setMsgDescription(rs.getString("msgDescription"));
				bean.setMsgId(rs.getInt("msgId"));
				bean.setMsgTitle(rs.getString("msgTitle"));
				list.add(bean);
			}

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			con.close();
			rs.close();
			stmt.close();
		}
		return list;

	}
}
