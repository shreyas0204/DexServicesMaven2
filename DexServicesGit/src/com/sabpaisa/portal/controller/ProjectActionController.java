package com.sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.OrganizationsProjectsBean;
import com.sabpaisa.portal.model.ProjectDetailsBean;
@RestController
public class ProjectActionController {

	@Autowired
	com.sabpaisa.portal.dao.OrganizationProjectDao projectDAO;
	
	@Autowired
	com.sabpaisa.portal.dao.OrganizationProductDao orgProductDao;
	
	@RequestMapping(value="/getListingsForProduct/{productName}/{productType}",method=RequestMethod.GET, headers = "Accept=application/json")
public @ResponseBody List<OrganizationsProjectsBean> getListingsForProduct(@PathVariable String productName,@PathVariable String productType){
		System.out.println("in side the getProjectBasedonProduct based on product ::::::::::::::::::: ");
		List<OrganizationsProjectsBean> beanList=null;
		
	OrganizationProductBean orgProBean=new OrganizationProductBean();
	orgProBean=orgProductDao.getProductTyeID(productName,productType);
		beanList=projectDAO.getProjectBasedOnProduct(orgProBean);
		System.out.println("Bean List Size ::::" +beanList);
		return beanList;
	}
	
	@RequestMapping(value="/getProjectDetails/{ProjectID}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<ProjectDetailsBean>getProjectDetails(@PathVariable Integer ProjectID){
		List<ProjectDetailsBean> detailsBean=null;
		try {
			detailsBean=projectDAO.getProjectDetails(ProjectID);
			return detailsBean;
		} catch (Exception e) {
			e.printStackTrace();
			return detailsBean;
		}
	}
}
