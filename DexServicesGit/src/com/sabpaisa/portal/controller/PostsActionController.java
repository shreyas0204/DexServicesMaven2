package com.sabpaisa.portal.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.GroupBean;
import com.sabpaisa.portal.model.OrganizationsBean;
import com.sabpaisa.portal.model.PostsBean;

@RestController
public class PostsActionController {
	@Autowired
	com.sabpaisa.portal.dao.PostsDao postsDAO;
	@Autowired
	com.sabpaisa.portal.dao.OrganizationDao OrganizationDao;
   @Autowired
   com.sabpaisa.portal.dao.GroupDao groupsDAO;
	@RequestMapping(value = "/getOrganizationPostsDetails/{organizationName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<PostsBean> getOrganizationPostsDetails(@PathVariable String organizationName) {
		System.out.println("Get Organization List::");
		//System.out.println("orgID List::" + orgID);
		List<PostsBean> postList = null;
		OrganizationsBean orgBean=new OrganizationsBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		Integer orgId=orgBean.getOrganizationId();
		

		postList = postsDAO.getOrganizationList(orgId);

		return postList;
	}

	@RequestMapping(value="/GetGroupPosts/{GroupName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<PostsBean>GetGroupPosts(@PathVariable String GroupName){
		List<PostsBean> postList = null;
		GroupBean grupBean=new GroupBean();
		grupBean=groupsDAO.getGroupID(GroupName);
		try {
			postList = postsDAO.getPostsList(grupBean);
			return postList;
		} catch (Exception e) {
			e.printStackTrace();
			return postList;
		}
	}
	
	@RequestMapping(value="/GetPosts", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<PostsBean>GetPosts(){
		List<PostsBean> postList = null;
		
		try {
			postList = postsDAO.getAllPostsList();
			return postList;
		} catch (Exception e) {
			e.printStackTrace();
			return postList;
		}
	}
	
	
	@RequestMapping(value = "/saveOrganizationPosts/{PostText}/{PostsAttachment}/{OrganizationName}/{GropuName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String saveOrganizationPosts(@PathVariable String PostText, @PathVariable String PostsAttachment,
			@PathVariable String OrganizationName , @PathVariable String GropuName) {
		System.out.println("Posts Texts::::::::::::::::" + PostText);
		System.out.println("Posts Attachment::::::::::::::::" + PostsAttachment);
		System.out.println("Posts Organization Name::::::::::::::::" + OrganizationName);

		String status = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String post_Date = dateFormat.format(date);

		System.out.println("Assign in variable::::::" + post_Date);

		PostsBean postsBean = new PostsBean();
		OrganizationsBean orgBean = new OrganizationsBean();
		orgBean = OrganizationDao.getOrganizationID(OrganizationName);
		GroupBean grupBean=new GroupBean();
		grupBean=groupsDAO.getGroupID(GropuName);
		
		postsBean.setGrpBean(grupBean);
		postsBean.setPosts_Text(PostText);
		postsBean.setPosts_Attachments(PostsAttachment);
		//postsBean.setOrgBean(orgBean);
		postsBean.setPost_Date(post_Date);
		try {
			postsDAO.savePostsData(postsBean);
			status = "SUccessfully Saved::::::::::::";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			status = "Fail";
			return status;
		}

	}

}
