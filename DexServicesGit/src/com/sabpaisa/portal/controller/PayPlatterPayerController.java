package com.sabpaisa.portal.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.payplatter.bean.MessageBean;
import com.payplatter.bean.PayerBean;
import com.payplatter.bean.TransactionBean;
import com.payplatter.dao.PayerDAO;

@RestController
public class PayPlatterPayerController {
	@RequestMapping(value = "/getPayerProfile/{Pid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public PayerBean getPayerProfile(@PathVariable String Pid) {
		PayerDAO payerDao = new PayerDAO();
		PayerBean payerBean = new PayerBean();

		try {
			payerBean = payerDao.getPayerProfile(Pid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payerBean;
	}

	@RequestMapping(value = "/getPayerTransactionMerchantWise/{Peid}/{Mid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<TransactionBean> getPayerTransactionMerchantWise(@PathVariable String Peid, @PathVariable String Mid) {
		PayerDAO payerDao = new PayerDAO();
		List<TransactionBean> payerList = new ArrayList<>();

		try {
			payerList = payerDao.getPayerTransactionMerchantWise(Peid, Mid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payerList;
	}

	@RequestMapping(value = "/getPayerTransaction/{Peid}/{kuchBhi}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<TransactionBean> getPayerTransaction(@PathVariable String Peid, @PathVariable String kuchBhi) {
		PayerDAO payerDao = new PayerDAO();
		List<TransactionBean> payerList = new ArrayList<>();

		try {
			payerList = payerDao.getPayerTransaction(Peid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return payerList;
	}

	@RequestMapping(value = "/getPayerMessages/{Peid}/{Mid}", method = RequestMethod.GET, headers = "Accept=application/json")
	public List<MessageBean> getPayerMessages(@PathVariable String Peid,@PathVariable String Mid) {
		PayerDAO payerDao = new PayerDAO();
		List<MessageBean> list = new ArrayList<>();

		try {
			list = payerDao.getPayerMessages(Peid, Mid);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return list;
	}

	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
		response.setHeader("Access-Control-Allow-Origin", "*");
	}
}
