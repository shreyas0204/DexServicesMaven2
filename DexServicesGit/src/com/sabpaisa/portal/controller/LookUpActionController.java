package com.sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.LookUpBean;
import com.sabpaisa.portal.model.OrganizationProductBean;


@RestController
public class LookUpActionController {
	@Autowired
	com.sabpaisa.portal.dao.LookUPProductDao lookUpDAO;
	
	@RequestMapping(value = "/getProductType", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<LookUpBean> getProductDetails(){
		System.out.println("Get Product Type List::");
		List<LookUpBean> lookupList=null;
		try {
			lookupList=lookUpDAO.getProductType();
			return lookupList;
		} catch (Exception e) {
		 System.out.println("In catch Block :::::::::");
		 e.printStackTrace();
		 return lookupList;
		}
		
		
		
		}
}
