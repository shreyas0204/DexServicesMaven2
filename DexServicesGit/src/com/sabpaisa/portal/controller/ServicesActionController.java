package com.sabpaisa.portal.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.OrganizationsBean;
import com.sabpaisa.portal.model.ServicesBean;

@RestController
public class ServicesActionController {

	@Autowired
	com.sabpaisa.portal.dao.ServicesDao servicesDAO;
	@Autowired
	com.sabpaisa.portal.dao.OrganizationProductDao orgProductDao;
	
	//Save Services
	@RequestMapping(value="/setServicesDetails/{ServicesName}/{productName}", method = RequestMethod.GET, headers = "Accept=application/json")
	public String setServicesDetails(@PathVariable String ServicesName,@PathVariable String productName){
		System.out.println("productName::"+ServicesName);
		
		ServicesBean servicesBean=new ServicesBean();
		OrganizationProductBean productBean=new OrganizationProductBean();
		//productBean.setProductId(productID);
		productBean=orgProductDao.getProductID(productName);
		
		servicesBean.setServices_description(ServicesName);
		servicesBean.setProductBean(productBean);
		String status=null;
		try {
			servicesDAO.saveServices(servicesBean);
			status="Successfully saved in DB";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			status="fail:::::::";
			return status;
		}
		
		}
	// END
	
	//Fetch Services Based on Product ID
	@RequestMapping(value = "/getServicesDetailsBasedOnProduct/{productId}", method = RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<ServicesBean> getServicesDetailsBasedOnProduct(@PathVariable Integer productId){
		System.out.println("Get Organization List::");
		List<ServicesBean> productList=null;
		productList=servicesDAO.getServicesList(productId);
		/*for (OrganizationsBean organizationBean:organizationList) {
			System.out.println("Organization List SIze :::"+organizationList.size());
		}*/
		return productList;
		}
	//END
	
	// Get Services Based on Product 
	@RequestMapping(value="/getServicesBasedonProduct/{productName}",method=RequestMethod.GET, headers = "Accept=application/json")
	public @ResponseBody List<ServicesBean>getProductBasedonUser(@PathVariable String productName){
		List<OrganizationProductBean> oregUsersBean=null;
	    List<ServicesBean> servicesBean=null;
		
		OrganizationProductBean productBean=new OrganizationProductBean();
		System.out.println("In side the getOrganizationBasedonUser::");
		productBean=orgProductDao.getProductID(productName);
		System.out.println("Product ID Based on Product Name::::::::::::" +productBean.getProductId());
		servicesBean=servicesDAO.getServicesLists(productBean);
		for (ServicesBean servicesBeans : servicesBean) {
			System.out.println("Bean Details ::::::" +servicesBeans.getServices_description());
		}
		return servicesBean;
	}
	
	//END
}
