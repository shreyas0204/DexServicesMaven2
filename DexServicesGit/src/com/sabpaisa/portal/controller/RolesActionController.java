package com.sabpaisa.portal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.OrganizationsBean;
import com.sabpaisa.portal.model.RolesBean;

@RestController
public class RolesActionController {

	@Autowired
	com.sabpaisa.portal.dao.RolesDao rolesDAO;
	@Autowired
	com.sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	
	
	@RequestMapping(value="/setRoles/{RolesName}/{Description}/{organizationName}",method=RequestMethod.GET, headers = "Accept=application/json")
	public String setOrganizationDetails(@PathVariable String RolesName,@PathVariable String Description,@PathVariable String organizationName){
		System.out.println("Role Name :::::"+RolesName);
		System.out.println("Descriptions  :::::"+Description);
		OrganizationsBean orgBean=new OrganizationsBean();
		RolesBean roleBean=new RolesBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		roleBean.setRole_Name(RolesName);
		roleBean.setRole_Descriptions(Description);
		roleBean.setOrgBean(orgBean);
		try {
			rolesDAO.setRoleDescriptions(roleBean);
			String status="Successfully Saved in DB";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			String status="Sorry";
			return status;
		}
		
		
	} 
}
