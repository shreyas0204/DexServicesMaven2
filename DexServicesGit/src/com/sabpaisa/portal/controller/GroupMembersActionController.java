package com.sabpaisa.portal.controller;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sabpaisa.portal.model.GroupBean;
import com.sabpaisa.portal.model.GroupMembersBean;
import com.sabpaisa.portal.model.OrganizationsBean;
import com.sabpaisa.portal.model.RolesBean;

@RestController
public class GroupMembersActionController {
 
	@Autowired
	com.sabpaisa.portal.dao.GroupMembersDao groupMembersDAO;
	@Autowired
	com.sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	
	
	@RequestMapping(value="/saveGroupMembers/{GroupName}/{groupType}/{organizationName}",method=RequestMethod.GET, headers = "Accept=application/json")
	public String saveGroupMembers(@PathVariable String GroupName,@PathVariable String groupType,@PathVariable String organizationName){
		
		
		OrganizationsBean orgBean=new OrganizationsBean();
		
		GroupBean groupBean=new GroupBean();
		GroupMembersBean groupMemberBean=new GroupMembersBean();
		orgBean=OrganizationDao.getOrganizationID(organizationName);
		
		
		
		
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		System.out.println(dateFormat.format(date));
		String created_Date=dateFormat.format(date);
		
		System.out.println("Assign in variable::::::" +created_Date);
		
		
		/*roleBean.setRole_Name(RolesName);
		roleBean.setRole_Descriptions(Description);
		roleBean.setOrgBean(orgBean);*/
		groupBean.setGroupName(GroupName);
		groupBean.setGroupType(groupType);
		groupBean.setCreated_Date(created_Date);
		groupBean.setOrgBean(orgBean);
		try {
			//groupMembersDAO.saveCreatedGroups(groupBean);
			String status="Successfully Saved in DB";
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			String status="Sorry";
			return status;
		}
		
		
	} 
	
	 
		@RequestMapping(value="/listOfGroupMembers/{orgID}",method=RequestMethod.GET, headers = "Accept=application/json")
		public List<GroupMembersBean> listOfGroupMembers(@PathVariable Integer orgID){
			List<GroupMembersBean>bean=null;
			try {
				bean=groupMembersDAO.getMemberList(orgID);
				return bean;
			} catch (Exception e) {
				e.printStackTrace();
				return bean;
			}
			
		}
	
	
}
