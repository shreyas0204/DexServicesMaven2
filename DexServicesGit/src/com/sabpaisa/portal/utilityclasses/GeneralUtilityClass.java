/**
 * 
 */
package com.sabpaisa.portal.utilityclasses;

/**
 * @author Aditya Mar 10, 2017
 */
public class GeneralUtilityClass {

	public static boolean isInteger(String s, int str_len) {//Method to check whether a string is an integer
		if (s.isEmpty())
			return false;
		for (int i = 0; i < s.length(); i++) {
			if (i == 0 && s.charAt(i) == '-') {
				if (s.length() == 1)
					return false;
				else
					continue;
			}
			if (s.length()!=str_len)
				return false;
		}
		return true;
	}
}
