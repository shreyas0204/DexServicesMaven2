package com.sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "features_access_grants_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class FeaturesAccessBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer featuresId;
	private String access_Type;
	
	//private String test;
		

	@ManyToOne(targetEntity = OrganizationProductBean.class)
	@JoinColumn(name = "product_id_fk", referencedColumnName = "productId")
	private OrganizationProductBean product;
	
	@ManyToOne(targetEntity = RolesBean.class)
	@JoinColumn(name = "role_id_fk", referencedColumnName = "roleId")
	private RolesBean role;
	
	
	public UsersBean getUser() {
		return user;
	}
	public void setUser(UsersBean user) {
		this.user = user;
	}
	@ManyToOne(targetEntity = UsersBean.class)
	@JoinColumn(name = "user_id_fk", referencedColumnName = "userId")
	private UsersBean user;
	public RolesBean getRole() {
		return role;
	}
	public void setRole(RolesBean role) {
		this.role = role;
	}
	public OrganizationProductBean getProduct() {
		return product;
	}
	public void setProduct(OrganizationProductBean product) {
		this.product = product;
	}
	public Integer getFeaturesId() {
		return featuresId;
	}
	public void setFeaturesId(Integer featuresId) {
		this.featuresId = featuresId;
	}
	public String getAccess_Type() {
		return access_Type;		
	}
	public void setAccess_Type(String access_Type) {
		this.access_Type = access_Type;
	}

}
