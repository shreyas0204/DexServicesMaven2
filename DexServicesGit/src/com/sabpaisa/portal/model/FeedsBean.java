package com.sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_feeds")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class FeedsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer feedId;
	@Column(unique=true)
	private String feed_Name;
	public String getFeed_Name() {
		return feed_Name;
	}
	public void setFeed_Name(String feed_Name) {
		this.feed_Name = feed_Name;
	}

	
	public OrganizationsBean getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}

	private String  feedText;
	
	@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean; 
	public Integer getFeedId() {
		return feedId;
	}
	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}
	public String getFeedText() {
		return feedText;
	}
	public void setFeedText(String feedText) {
		this.feedText = feedText;
	}

}
