package com.sabpaisa.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "org_execution_category_lookup")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class OrgExecutionsBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer ID;
	@Column(unique = true)
	private String category_ID;
	private String category_descriptions;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getCategory_ID() {
		return category_ID;
	}
	public void setCategory_ID(String category_ID) {
		this.category_ID = category_ID;
	}
	public String getCategory_descriptions() {
		return category_descriptions;
	}
	public void setCategory_descriptions(String category_descriptions) {
		this.category_descriptions = category_descriptions;
	}
}
