package com.sabpaisa.portal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "industries_lookup")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class IndustriesLookUpBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer industries_ID;
	private String industries_descriptions;
	public Integer getIndustries_ID() {
		return industries_ID;
	}
	public void setIndustries_ID(Integer industries_ID) {
		this.industries_ID = industries_ID;
	}
	public String getIndustries_descriptions() {
		return industries_descriptions;
	}
	public void setIndustries_descriptions(String industries_descriptions) {
		this.industries_descriptions = industries_descriptions;
	}
}
