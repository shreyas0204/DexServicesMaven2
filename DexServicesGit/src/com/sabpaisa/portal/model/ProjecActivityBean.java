package com.sabpaisa.portal.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "project_activites")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class ProjecActivityBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer activityId;
	private String updatedBY;
	private String entryDate;
	private String isMilestone;
	private Integer parentActivity_ID;
	private String comments;
	
	@ManyToOne(cascade=CascadeType.ALL,targetEntity=OrganizationsProjectsBean.class)
	@JoinColumn(name="project_IdFk",referencedColumnName="project_ID")
	private OrganizationsProjectsBean projectBean;
	
	@ManyToOne(cascade=CascadeType.ALL,targetEntity=ProjectPhasesBean.class)
	@JoinColumn(name="phase_IdFk",referencedColumnName="phases_ID")
	private ProjectPhasesBean projectPhasesBean;
	
	public ProjectPhasesBean getProjectPhasesBean() {
		return projectPhasesBean;
	}
	public void setProjectPhasesBean(ProjectPhasesBean projectPhasesBean) {
		this.projectPhasesBean = projectPhasesBean;
	}
	//ProjectPhasesBean
	public OrganizationsProjectsBean getProjectBean() {
		return projectBean;
	}
	public void setProjectBean(OrganizationsProjectsBean projectBean) {
		this.projectBean = projectBean;
	}
	public Integer getActivityId() {
		return activityId;
	}
	public void setActivityId(Integer activityId) {
		this.activityId = activityId;
	}
	public String getUpdatedBY() {
		return updatedBY;
	}
	public void setUpdatedBY(String updatedBY) {
		this.updatedBY = updatedBY;
	}
	public String getEntryDate() {
		return entryDate;
	}
	public void setEntryDate(String entryDate) {
		this.entryDate = entryDate;
	}
	public String getIsMilestone() {
		return isMilestone;
	}
	public void setIsMilestone(String isMilestone) {
		this.isMilestone = isMilestone;
	}
	public Integer getParentActivity_ID() {
		return parentActivity_ID;
	}
	public void setParentActivity_ID(Integer parentActivity_ID) {
		this.parentActivity_ID = parentActivity_ID;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
}
