package com.sabpaisa.portal.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_social_groups")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class GroupBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer groupId;
	private String groupType;
	private String groupName;
	private String created_Date;
	private String groupDescription;
	
	
	@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "groupBean", fetch = FetchType.EAGER)
	private Set<GroupMembersBean> groupMember;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL,mappedBy="grpBean",fetch=FetchType.EAGER) 
	private Set<PostsBean> postBean;
	
	public OrganizationsBean getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}
	public Integer getGroupId() {
		return groupId;
	}
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	public String getGroupType() {
		return groupType;
	}
	public void setGroupType(String groupType) {
		this.groupType = groupType;
	}
	public String getGroupDescription() {
		return groupDescription;
	}
	public void setGroupDescription(String groupDescription) {
		this.groupDescription = groupDescription;
	}
	public String getCreated_Date() {
		return created_Date;
	}
	public void setCreated_Date(String created_Date) {
		this.created_Date = created_Date;
	}
	
	
	public Set<GroupMembersBean> getGroupMember() {
		return groupMember;
	}
	public void setGroupMember(Set<GroupMembersBean> groupMember) {
		this.groupMember = groupMember;
	}

	public Set<PostsBean> getPostBean() {
		return postBean;
	}
	public void setPostBean(Set<PostsBean> postBean) {
		this.postBean = postBean;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
}
