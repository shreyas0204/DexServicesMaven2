package com.sabpaisa.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "test_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class TestBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer testId;
	@Column(unique = true)
	private String organization_name;
	private String relationship_type;
	public Integer getTestId() {
		return testId;
	}
	public void setTestId(Integer testId) {
		this.testId = testId;
	}
	public String getOrganization_name() {
		return organization_name;
	}
	public void setOrganization_name(String organization_name) {
		this.organization_name = organization_name;
	}
	public String getRelationship_type() {
		return relationship_type;
	}
	public void setRelationship_type(String relationship_type) {
		this.relationship_type = relationship_type;
	}

}
