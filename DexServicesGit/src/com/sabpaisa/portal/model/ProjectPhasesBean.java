package com.sabpaisa.portal.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "project_phases")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class ProjectPhasesBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer Id;
	private Integer phases_ID;
	@Column(nullable=false)
	private String descriptions;
	private String phaseStartDate,phaseEndDate,status,comments;
	
	@ManyToOne(cascade=CascadeType.ALL,targetEntity=OrganizationsProjectsBean.class)
	@JoinColumn(name="project_IdFk",referencedColumnName="project_ID")
	private OrganizationsProjectsBean projectBean;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "projectPhasesBean", fetch = FetchType.EAGER)
	private Set<ProjecActivityBean> activityBean;

	public Set<ProjecActivityBean> getActivityBean() {
		return activityBean;
	}
	public void setActivityBean(Set<ProjecActivityBean> activityBean) {
		this.activityBean = activityBean;
	}
	public OrganizationsProjectsBean getProjectBean() {
		return projectBean;
	}
	public void setProjectBean(OrganizationsProjectsBean projectBean) {
		this.projectBean = projectBean;
	}
	public Integer getId() {
		return Id;
	}
	public void setId(Integer id) {
		Id = id;
	}
	public Integer getPhases_ID() {
		return phases_ID;
	}
	public void setPhases_ID(Integer phases_ID) {
		this.phases_ID = phases_ID;
	}
	public String getDescriptions() {
		return descriptions;
	}
	public void setDescriptions(String descriptions) {
		this.descriptions = descriptions;
	}
	public String getPhaseStartDate() {
		return phaseStartDate;
	}
	public void setPhaseStartDate(String phaseStartDate) {
		this.phaseStartDate = phaseStartDate;
	}
	public String getPhaseEndDate() {
		return phaseEndDate;
	}
	public void setPhaseEndDate(String phaseEndDate) {
		this.phaseEndDate = phaseEndDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}

}
