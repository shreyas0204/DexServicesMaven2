package com.sabpaisa.portal.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_roles")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class RolesBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer roleId;
	
	private String role_Descriptions;
	
	private String role_Name;
	public String getRole_Name() {
		return role_Name;
	}
	public void setRole_Name(String role_Name) {
		this.role_Name = role_Name;
	}
	

	
	@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;
	
	public Set<FeaturesAccessBean> getFeature() {
		return feature;
	}
	public void setFeature(Set<FeaturesAccessBean> feature) {
		this.feature = feature;
	}
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "role", fetch = FetchType.EAGER)
	private Set<FeaturesAccessBean> feature;
	public OrganizationsBean getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}
	public Integer getRoleId() {
		return roleId;
	}
	public void setRoleId(Integer roleId) {
		this.roleId = roleId;
	}
	public String getRole_Descriptions() {
		return role_Descriptions;
	}
	public void setRole_Descriptions(String role_Descriptions) {
		this.role_Descriptions = role_Descriptions;
	}

}
