package com.sabpaisa.portal.model;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_product")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class OrganizationProductBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer productId;
	@Column(unique = true)
	private String productdesctiption;

	private String productType;

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "productBean", fetch = FetchType.EAGER)
	private Set<ServicesBean> service;

	/*@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;*/

	/*@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "orgBean", fetch = FetchType.EAGER)
	private Set<OrganizationsProjectsBean> project;*/
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "prodBean", fetch = FetchType.EAGER)
	private Set<OrganizationsProjectsBean> projectType;
	
	public Set<OrganizationsProjectsBean> getProjectType() {
		return projectType;
	}

	public void setProjectType(Set<OrganizationsProjectsBean> projectType) {
		this.projectType = projectType;
	}

	public Set<ServicesBean> getService() {
		return service;
	}

	public void setService(Set<ServicesBean> service) {
		this.service = service;
	}

	/*public OrganizationsBean getOrgBean() {
		return orgBean;
	}

	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}*/

	public Set<FeaturesAccessBean> getFeature() {
		return feature;
	}

	public void setFeature(Set<FeaturesAccessBean> feature) {
		this.feature = feature;
	}

	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "product", fetch = FetchType.EAGER)
	private Set<FeaturesAccessBean> feature;

	public Integer getProductId() {
		return productId;
	}

	public void setProductId(Integer productId) {
		this.productId = productId;
	}

	public String getProductdesctiption() {
		return productdesctiption;
	}

	public void setProductdesctiption(String productdesctiption) {
		this.productdesctiption = productdesctiption;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}
}
