package com.sabpaisa.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "document_types_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class DocumentTypesBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer ID;
	@Column(unique = true)
	private String documents_type;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getDocuments_type() {
		return documents_type;
	}
	public void setDocuments_type(String documents_type) {
		this.documents_type = documents_type;
	}
	

}
