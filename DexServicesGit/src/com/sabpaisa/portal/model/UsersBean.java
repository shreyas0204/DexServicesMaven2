package com.sabpaisa.portal.model;

import java.util.List;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;


@Entity
@Table(name = "users_master")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class UsersBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer userId;

	
	@JsonProperty
	private String firstName, lastName, dateOfBirth,emailID,contactNumber,address;
	
	@OneToOne(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	LoginBean loginBean;
	
	public Set<RolesBean> getUsersrolesBean() {
		return usersrolesBean;
	}
	public void setUsersrolesBean(Set<RolesBean> usersrolesBean) {
		this.usersrolesBean = usersrolesBean;
	}
	/*@OneToOne(cascade = CascadeType.ALL, targetEntity = LoginBean.class, mappedBy = "userBean")
	private LoginBean loginBean;*/
	@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "users_roles", joinColumns = @JoinColumn(name = "userId"), inverseJoinColumns = @JoinColumn(name = "roleId"))
    private Set<RolesBean> usersrolesBean;
	
	public Set<GroupMembersBean> getGroupMember() {
		return groupMember;
	}
	public void setGroupMember(Set<GroupMembersBean> groupMember) {
		this.groupMember = groupMember;
	}
	@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "user", fetch = FetchType.EAGER)
	private Set<FeaturesAccessBean> feature;
	
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "userBean", fetch = FetchType.EAGER)
	private Set<GroupMembersBean> groupMember;
		public Integer getUserId() {
		return userId;
	}
public Set<FeaturesAccessBean> getFeature() {
			return feature;
		}
		public void setFeature(Set<FeaturesAccessBean> feature) {
			this.feature = feature;
		}
public OrganizationsBean getOrgBean() {
			return orgBean;
		}
		public void setOrgBean(OrganizationsBean orgBean) {
			this.orgBean = orgBean;
		}	
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getEmailID() {
		return emailID;
	}
	public void setEmailID(String emailID) {
		this.emailID = emailID;
	}
	public String getContactNumber() {
		return contactNumber;
	}
	public void setContactNumber(String contactNumber) {
		this.contactNumber = contactNumber;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}	
	public LoginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
}
