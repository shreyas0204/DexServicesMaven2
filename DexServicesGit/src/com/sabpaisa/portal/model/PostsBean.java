package com.sabpaisa.portal.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organization_posts")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class PostsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer postsID;
	private String posts_Text,posts_Attachments,post_Date;
	

	/*@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;*/
	
	@ManyToOne(targetEntity=GroupBean.class)
	@JoinColumn(name="group_id_fk", referencedColumnName="groupId")
	private GroupBean grpBean;
	
	
	public GroupBean getGrpBean() {
		return grpBean;
	}
	public void setGrpBean(GroupBean grpBean) {
		this.grpBean = grpBean;
	}
	/*public OrganizationsBean getOrgBean() {
		return orgBean;
	}
	public void setOrgBean(OrganizationsBean orgBean) {
		this.orgBean = orgBean;
	}*/
	public Integer getPostsID() {
		return postsID;
	}
	public void setPostsID(Integer postsID) {
		this.postsID = postsID;
	}
	public String getPosts_Text() {
		return posts_Text;
	}
	public void setPosts_Text(String posts_Text) {
		this.posts_Text = posts_Text;
	}
	public String getPosts_Attachments() {
		return posts_Attachments;
	}
	public void setPosts_Attachments(String posts_Attachments) {
		this.posts_Attachments = posts_Attachments;
	}
	public String getPost_Date() {
		return post_Date;
	}
	public void setPost_Date(String post_Date) {
		this.post_Date = post_Date;
	}

}
