package com.sabpaisa.portal.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "organizations_projects")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class OrganizationsProjectsBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer project_ID;
	private String project_Name;
    private String project_Type;
	
	
	public PartnerDetailsBean getPartnerDetailsBean() {
		return partnerDetailsBean;
	}
	public void setPartnerDetailsBean(PartnerDetailsBean partnerDetailsBean) {
		this.partnerDetailsBean = partnerDetailsBean;
	}
	public String getProject_Type() {
		return project_Type;
	}
	public void setProject_Type(String project_Type) {
		this.project_Type = project_Type;
	}
		
	/*@ManyToOne(targetEntity = OrganizationsBean.class)
	@JoinColumn(name = "organization_id_fk", referencedColumnName = "organizationId")
	private OrganizationsBean orgBean;
	*/
	
	@ManyToOne(targetEntity = OrganizationProductBean.class)
	@JoinColumn(name = "product_id_fk", referencedColumnName = "productId")
	private OrganizationProductBean prodBean;
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "projectBean", fetch = FetchType.EAGER)
	private Set<ProjectPhasesBean> projectPhaseBean;
	
	
	
	public OrganizationProductBean getProdBean() {
		return prodBean;
	}
	public void setProdBean(OrganizationProductBean prodBean) {
		this.prodBean = prodBean;
	}
	public Set<ProjectPhasesBean> getProjectPhaseBean() {
		return projectPhaseBean;
	}
	public void setProjectPhaseBean(Set<ProjectPhasesBean> projectPhaseBean) {
		this.projectPhaseBean = projectPhaseBean;
	}
	public Set<ProjecActivityBean> getProjActivityBean() {
		return projActivityBean;
	}
	public void setProjActivityBean(Set<ProjecActivityBean> projActivityBean) {
		this.projActivityBean = projActivityBean;
	}
	@JsonIgnore
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "projectBean", fetch = FetchType.EAGER)
	private Set<ProjecActivityBean> projActivityBean;
	
	@JsonIgnore
	@OneToOne(cascade=CascadeType.ALL)
	private PartnerDetailsBean partnerDetailsBean;
	
	public Integer getProject_ID() {
		return project_ID;
	}
	public void setProject_ID(Integer project_ID) {
		this.project_ID = project_ID;
	}
	public String getProject_Name() {
		return project_Name;
	}
	public void setProject_Name(String project_Name) {
		this.project_Name = project_Name;
	}
	
	
}
