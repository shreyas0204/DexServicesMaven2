package com.sabpaisa.portal.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "document_uploads")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class DocumentsUploadBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer  ID;
	private String documents_path,status,review_date,review_needed_date,reviewed_doc_path;
	public Integer getID() {
		return ID;
	}
	public void setID(Integer iD) {
		ID = iD;
	}
	public String getDocuments_path() {
		return documents_path;
	}
	public void setDocuments_path(String documents_path) {
		this.documents_path = documents_path;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReview_date() {
		return review_date;
	}
	public void setReview_date(String review_date) {
		this.review_date = review_date;
	}
	public String getReview_needed_date() {
		return review_needed_date;
	}
	public void setReview_needed_date(String review_needed_date) {
		this.review_needed_date = review_needed_date;
	}
	public String getReviewed_doc_path() {
		return reviewed_doc_path;
	}
	public void setReviewed_doc_path(String reviewed_doc_path) {
		this.reviewed_doc_path = reviewed_doc_path;
	}
	

}
