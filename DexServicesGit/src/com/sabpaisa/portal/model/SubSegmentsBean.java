package com.sabpaisa.portal.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@Entity
@Table(name = "subsegments_lookup")
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonDeserialize
public class SubSegmentsBean {
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer subsegment_ID;
	private String subsegments_descriptions;
	
	// Join Table Between Sub-Segment And Compliance Many to many relationship 
	
	@ManyToMany(cascade = CascadeType.ALL,fetch=FetchType.EAGER)
	@JoinTable(name = "subsegment_complaince_mapping", joinColumns = @JoinColumn(name = "subsegment_ID"), inverseJoinColumns = @JoinColumn(name = "compliance_ID"))
	private Set<ComplainceLookUpBean> complainceBean;

	public Integer getSubsegment_ID() {
		return subsegment_ID;
	}
	public void setSubsegment_ID(Integer subsegment_ID) {
		this.subsegment_ID = subsegment_ID;
	}
	public String getSubsegments_descriptions() {
		return subsegments_descriptions;
	}
	public void setSubsegments_descriptions(String subsegments_descriptions) {
		this.subsegments_descriptions = subsegments_descriptions;
	}

}
