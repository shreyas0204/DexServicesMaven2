package com.sabpaisa.portal.dao;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.FileUploadBean;

public class FileUploadDao {
	@Autowired
	private SessionFactory sessionFactory;
	public void save(FileUploadBean fileUploadBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			//System.out.println("In side the Save File For Upload the file ...........................");
			session.save(fileUploadBean);
			transaction.commit();
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			session.close();
		}
		
	}
	public int saveFilerecord(FileUploadBean fileBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		int result=0;
		try {
			System.out.println("In side the Save File For Upload the file ...........................");
			result=(int) session.save(fileBean);
			transaction.commit();
			return result;
	}catch (Exception e) {
		e.printStackTrace();
		return result;
	}finally {
		session.close();
	}
	
	}

}
