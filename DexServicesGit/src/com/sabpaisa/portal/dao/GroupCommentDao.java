package com.sabpaisa.portal.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.GroupCommentsBean;

public class GroupCommentDao {
	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<GroupCommentsBean> getMessageBasedOnUserID(Integer UserID) {
		Session session = sessionFactory.openSession();
		List<GroupCommentsBean>bean=new ArrayList<GroupCommentsBean>();
		try {
			System.out.println("Inside the getMessageBasedOnUserID of DAO ::::::::::::::::::; ");
			Criteria criteria=session.createCriteria(GroupCommentsBean.class);
			bean=(List<GroupCommentsBean>) criteria.add(Restrictions.eq("userId", UserID)).list();
			bean.size();
			System.out.println("Bean Size :::::::::" +bean.size());
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}finally {
			session.close();
		}
		
	}
	

}
