package com.sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.mysql.jdbc.log.Log;
import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.OrganizationsProjectsBean;

public class OrganizationProductDao {
	@Autowired
	private SessionFactory sessionFactory;

	/*@SuppressWarnings("unchecked")
	public List<OrganizationProductBean> getProductList() {
		Session session = sessionFactory.openSession();
		List<OrganizationProductBean> productBeanList = null;
		try {
			Criteria criteria = session.createCriteria(OrganizationProductBean.class).setResultTransformer(
					Criteria.DISTINCT_ROOT_ENTITY);
			productBeanList = (List<OrganizationProductBean>) criteria.list();
			return productBeanList;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("");
			return null;
		} finally {
			session.close();
		}

	}
*/
	@SuppressWarnings("unchecked")
	public List<OrganizationProductBean> getOrgProductList(Integer orgID) {
		Session session = sessionFactory.openSession();
		List<OrganizationProductBean> productBeanList = null;
		try {
			Criteria criteria = session.createCriteria(OrganizationProductBean.class);
			criteria.add(Restrictions.eq("orgBean.organizationId", orgID)).setResultTransformer(criteria.DISTINCT_ROOT_ENTITY);
			// criteria.add(Restrictions.eq("productId", orgID));
			// productBeanList = (List<OrganizationProductBean>)
			// criteria.list();
			productBeanList = criteria.list();
			System.out.println("List size is :::::" + productBeanList.size());
			return productBeanList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public Integer saveProductDetails(OrganizationProductBean productBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			System.out.println("In side the saveUserDetails in user master....................................");
			OrganizationProductBean data = new OrganizationProductBean();
			int prodID = (int) session.save(productBean);
			transaction.commit();
			return prodID;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}

	public OrganizationProductBean getProductID(String productName) {
		Session session = sessionFactory.openSession();
		OrganizationProductBean productBean = null;
		try {
			productBean = (OrganizationProductBean) session.createCriteria(OrganizationProductBean.class)
					.add(Restrictions.eq("productdesctiption", productName)).list().iterator().next();
		} catch (NullPointerException ec) {

			ec.printStackTrace();

		} finally {
			session.close();
		}
		return productBean;
	}

	public List<OrganizationProductBean> getProductUsers(Integer userID) {
		Session session = sessionFactory.openSession();
		List<OrganizationProductBean> orgProduct = null;
		try {
			System.out.println("In side the DAO Method:::::::::::::::");

			String query = "SELECT * FROM sabpaisaportal.organization_product where productId "
					+ "in(SELECT organizationId FROM sabpaisaportal.organizations_users where userId=" + userID + ")";

			SQLQuery sqlQuery = session.createSQLQuery(query);
			orgProduct=sqlQuery.list();
			return orgProduct;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		finally {
			session.close();
		}
	}
	@SuppressWarnings("unchecked")
	@Transactional
	public List<OrganizationProductBean> getProductList() {
		List<OrganizationProductBean> bean=null;
		Session session=sessionFactory.openSession();
		try {
			Criteria criteria=session.createCriteria(OrganizationProductBean.class);
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			bean=criteria.list();			
			System.out.println("Product Size ::::::" +bean.size());
		} catch (Exception e) {
		 System.out.println("in side Exception");
		}
		finally{
			session.close();
		}
		return bean;
	}

	public OrganizationProductBean getProductTyeID(String productName, String productType) {
		OrganizationProductBean bean=new OrganizationProductBean();
		Session session=sessionFactory.openSession();
		try {
			bean=(OrganizationProductBean) session.createCriteria(OrganizationProductBean.class).add(Restrictions.eq("productdesctiption", productName))
					.add(Restrictions.eq("productType", productType)).list().iterator().next();
			System.out.println("Ben ID:::" +bean.getProductId());
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return bean;
		}
		finally{
			session.close();
		}
		
	}

	/*public List<OrganizationProductBean> getProjectBasedOnProduct(String productName, String productType) {
		List<OrganizationProductBean> bean=null;
		Session session=sessionFactory.openSession();
		try {
			System.out.println("in side the getProjectBasedOnProduct dao::::::::::::");
			Criteria criteria=session.createCriteria(OrganizationProductBean.class);
			criteria.add(Restrictions.eq("productdesctiption", productName));
			criteria.add(Restrictions.eq("productType", productType));
			criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
			bean=criteria.list();
			System.out.println("Bean size is ::::" +bean.size());
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
			return bean;
		}
		finally{
			session.close();
		}
		
	}*/
}
