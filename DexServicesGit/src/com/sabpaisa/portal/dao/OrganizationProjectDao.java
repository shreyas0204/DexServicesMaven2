package com.sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.OrganizationsProjectsBean;
import com.sabpaisa.portal.model.ProjectDetailsBean;

public class OrganizationProjectDao {
	@Autowired
	private SessionFactory sessionFactory;

	public List<OrganizationsProjectsBean> getProjectBasedOnProduct(OrganizationProductBean orgProBean) {
		Session session=sessionFactory.openSession();
		List<OrganizationsProjectsBean> projectBen=null;
		try {
			Criteria criteria=session.createCriteria(OrganizationsProjectsBean.class);
			/*projectBen=(List<OrganizationsProjectsBean>) criteria.add(Restrictions.eq("project_ID", orgProBean.getProductId())).
					setResultTransformer(criteria.DISTINCT_ROOT_ENTITY).list();*/
			projectBen=(List<OrganizationsProjectsBean>) criteria.add(Restrictions.eq("prodBean.productId", orgProBean.getProductId())).
					setResultTransformer(criteria.DISTINCT_ROOT_ENTITY).list();
		//projectBen=(List<OrganizationsProjectsBean>) criteria.list().iterator().next();
		System.out.println("Bean Size :::" +projectBen.size());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return projectBen;
	}

	@SuppressWarnings("unchecked")
	public List<ProjectDetailsBean> getProjectDetails(Integer projectID) {
		Session session=sessionFactory.openSession();
		List<ProjectDetailsBean> bean=null;
		try {
			Criteria criteria=session.createCriteria(ProjectDetailsBean.class);
			bean=(List<ProjectDetailsBean>) criteria.add(Restrictions.eq("orgProjectBean.project_ID", projectID))
					.setResultTransformer(criteria.DISTINCT_ROOT_ENTITY).list();
			System.out.println("Bean Size :::::" +bean.size());
			return bean;
		} catch (Exception e) {
			e.printStackTrace();
			return bean;
		}
		finally{
			session.close();
		}
	}

	
}
