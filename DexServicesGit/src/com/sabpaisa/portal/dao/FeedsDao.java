package com.sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.FeedsBean;
import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.ServicesBean;

public class FeedsDao {
	@Autowired
	private SessionFactory sessionFactory;

	public Integer saveFeedsDetails(FeedsBean feedBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			System.out.println("In side the saveUserDetails in user master....................................");

			int prodID = (int) session.save(feedBean);
			transaction.commit();
			return prodID;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();

		}
	}

	@SuppressWarnings("unchecked")
	public List<FeedsBean> getOrgProductList(Integer orgId) {
		Session session = sessionFactory.openSession();
		List<FeedsBean> feedList = null;
		try {
			feedList = (List<FeedsBean>) session.createCriteria(FeedsBean.class)
					.add(Restrictions.eq("orgBean.organizationId", orgId))
					.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY).list();

			System.out.println("List size is :::::" + feedList.size());
			return feedList;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}

	}
}
