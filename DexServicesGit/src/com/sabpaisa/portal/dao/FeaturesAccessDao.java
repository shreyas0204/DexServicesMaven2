package com.sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.FeaturesAccessBean;
import com.sabpaisa.portal.model.OrganizationProductBean;

public class FeaturesAccessDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	public List<FeaturesAccessBean> getProductList() {
		Session session = sessionFactory.openSession();
		List<FeaturesAccessBean> featureBean=null;
		try {
			Criteria criteria = session.createCriteria(FeaturesAccessBean.class).setResultTransformer(
					Criteria.DISTINCT_ROOT_ENTITY);
			featureBean=(List<FeaturesAccessBean>)criteria.list();
			return featureBean;
		} catch (Exception e) {
			e.printStackTrace();
			return featureBean;
		}
		finally{
			session.close();
		}
	}

	public List<FeaturesAccessBean> getProductListBasedOnCriteria(Integer userID, Integer productID) {
		Session session = sessionFactory.openSession();
		List<FeaturesAccessBean> featureBean=null;
		try {
			Criteria criteria = session.createCriteria(FeaturesAccessBean.class);
			criteria.add(Restrictions.eq("product_id_fk", productID))
			.add(Restrictions.eq("user_id_fk", userID));
			//featureBean=(List<FeaturesAccessBean>)criteria.list();
			featureBean=criteria.list();
		return featureBean;
		}  catch (Exception e) {
			e.printStackTrace();
			return featureBean;
		}
		finally{
			session.close();
		}
	}

	public List<FeaturesAccessBean> getProductListBasedOnProduct(Integer productID) {
		Session session = sessionFactory.openSession();
		List<FeaturesAccessBean> featureBean=null;
		try {
			Criteria criteria = session.createCriteria(FeaturesAccessBean.class);
			criteria.add(Restrictions.eq("product_id_fk", productID));
			featureBean=(List<FeaturesAccessBean>)criteria.list();
			return featureBean;
		}  catch (Exception e) {
			e.printStackTrace();
			return featureBean;
		}
		finally{
			session.close();
		}

		
	}

}
