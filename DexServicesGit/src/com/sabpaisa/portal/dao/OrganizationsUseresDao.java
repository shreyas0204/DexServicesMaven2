package com.sabpaisa.portal.dao;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sabpaisa.portal.model.OrganizationsBean;
import com.sabpaisa.portal.model.OrganizationsUsersBean;
import com.sabpaisa.portal.model.UsersBean;

public class OrganizationsUseresDao {
	@Autowired
	private SessionFactory sessionFactory;
	@Autowired
	com.sabpaisa.portal.dao.OrganizationDao OrganizationDao;
	static Logger log = Logger.getLogger(OrganizationsUseresDao.class);
	@SuppressWarnings("unchecked")
	public List<OrganizationsBean> getOrganizationUsers(Integer userID) {
		Session session = sessionFactory.openSession();
		List<OrganizationsBean> orgsList = new ArrayList<OrganizationsBean>();
		List<OrganizationsUsersBean> orgsUsersList= new ArrayList<OrganizationsUsersBean>();
		//List<UsersBean> user = null;
		OrganizationDao orgdao = new OrganizationDao();
		try {
			System.out.println("Inside the DAO Method:::::::::::::::");

			Criteria cr = session.createCriteria(OrganizationsUsersBean.class);
			cr.add(Restrictions.eq("userId", userID));
			orgsUsersList = cr.list();
			log.info("Got "+orgsUsersList.size()+" ids from database for user id "+userID);
			if (orgsUsersList.size() > 0) {
				List<Integer> orgIds = new ArrayList<Integer>();
				for (int i = 0; i < orgsUsersList.size(); i++) {
					orgIds.add(orgsUsersList.get(i).getOrganizationId());
				}
				orgsList = OrganizationDao.getOrganizationByID(orgIds);
			}

			// String
			// query="SELECT * FROM sabpaisaportal.organizations_users where userId="+userID+"";
			// ResultSet rs = session.createQuery(query);
			// SQLQuery sqlQuery = session.createSQLQuery(query);

			// System.out.println("First result :::"
			// +sqlQuery.getFirstResult());
			// List data=sqlQuery.list();

			/*
			 * oregUsersBean=sqlQuery.list();
			 * 
			 * 
			 * oregUsersBean = (List<OrganizationsBean>)
			 * session.createCriteria(OrganizationsBean.class)
			 * .add(Restrictions.eq("orguserBean.userId",userID))
			 * .list().iterator() .next();
			 * 
			 * System.out.println("Size of Orgaization :::::"+oregUsersBean.size(
			 * )); writeListToJsonArray(oregUsersBean);
			 */// Gson gson = new Gson();
				// convert your list to json
			// String jsonProdList = gson.toJson(oregUsersBean);
			// print your generated json
			/*
			 * String jsonProdList = ""; System.out.println("jsonProdList: " +
			 * jsonProdList);
			 */

			return orgsList;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("");
			return orgsList;
		} finally {
			session.close();
		}

	}

	public void writeListToJsonArray(List<OrganizationsBean> orgBeanList) throws IOException {
		// final List<Event> list = new ArrayList<Event>(2);
		// list.add(new Event("a1","a2"));
		// list.add(new Event("b1","b2"));

		final OutputStream out = new ByteArrayOutputStream();
		final ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		String arrayToJson = mapper.writeValueAsString(orgBeanList);
		System.out.println(" Convert List of person objects to JSON :" + arrayToJson);
		String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(orgBeanList);
		System.out.println(jsonInString);
		// mapper.writeValue(out, orgBeanList);
		// System.out.println(new String(data));
		// final byte[] data = ((ByteArrayOutputStream) out).toByteArray();
		// System.out.println(new String(data));
	}

}
