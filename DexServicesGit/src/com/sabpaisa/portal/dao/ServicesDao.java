package com.sabpaisa.portal.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.model.OrganizationProductBean;
import com.sabpaisa.portal.model.ServicesBean;

public class ServicesDao {
	@Autowired
	private SessionFactory sessionFactory;

	public Integer saveServices(ServicesBean servicesBean) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Integer servicesID = null;
		try {
			System.out.println("In side the saveUserDetails in user master....................................");
			ServicesBean data = new ServicesBean();
			servicesID = (int) session.save(servicesBean);
			transaction.commit();
			return servicesID;
		} catch (Exception ex) {
			ex.printStackTrace();
			System.out.println("fail");
			return servicesID;
		} finally {
			session.close();
		}
	}

	public List<ServicesBean> getServicesList(Integer productId) {
		Session session = sessionFactory.openSession();
		List<ServicesBean> servicesBean = null;
		try {
			Criteria criteria = session.createCriteria(ServicesBean.class);
			criteria.add(Restrictions.eq("product_id_fk", productId));
			servicesBean = (List<ServicesBean>) criteria.list();
			System.out.println("List size is :::::" + servicesBean.size());
			return servicesBean;
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		} finally {
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public List<ServicesBean> getServicesLists(OrganizationProductBean productBean) {
		Session session = sessionFactory.openSession();
		List<ServicesBean> servicesBean = null;
		try {

			Criteria criteria = session.createCriteria(ServicesBean.class);
			servicesBean = criteria.add(Restrictions.eq("product_id_fk", productBean.getProductId()))
					.setResultTransformer(CriteriaSpecification.DISTINCT_ROOT_ENTITY).list();

			System.out.println("Bean Size ::::" + servicesBean.size());
			return servicesBean;
		} catch (Exception e) {
			e.printStackTrace();
			return servicesBean;
		} finally {
			session.close();
		}
	}

	
	/*
	 * @SuppressWarnings("unchecked") public List<ServicesBean>
	 * getServicesList(OrganizationProductBean productBean) { Session session =
	 * sessionFactory.openSession(); List<ServicesBean> servicesBean = null; try
	 * {
	 * 
	 * String query =
	 * "SELECT * FROM sabpaisaportal.organization_services where product_id_fk="
	 * + productBean.getProductId() + " "; SQLQuery sqlQuery =
	 * session.createSQLQuery(query); servicesBean = sqlQuery.list();
	 * 
	 * System.out.println("List size is :::::" + servicesBean.size()); return
	 * servicesBean; } catch (Exception e) { e.printStackTrace(); return null; }
	 * finally { session.close(); } }
	 */

}
