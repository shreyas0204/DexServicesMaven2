package com.sabpaisa.portal.dao;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.List;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;

import org.hibernate.Criteria;
import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.LogicalExpression;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;

import com.sabpaisa.portal.enPasswordGenerator.PasswordEncryption;
import com.sabpaisa.portal.model.LoginBean;
import com.sabpaisa.portal.model.UsersBean;

public class LoginDao {
	@Autowired
	private SessionFactory sessionFactory;

	public void saveCredentials(String userName, String password, int reUserID) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		try {
			System.out.println("In side the saveCredentials in user master....................................");
			LoginBean data = new LoginBean();
			data.setUserName(userName);
			data.setPassword(password);
			// data.setUser_id_fk(reUserID);
			// data.set
			// data.setUserBean(session.get(UsersBean.class, reUserID ));
			session.save(data);

			transaction.commit();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			session.close();
		}
	}

	/*
	 * @SuppressWarnings("unchecked") public List<LoginBean>
	 * validateLoginCredentialsDao(String userName, String password) throws
	 * InvalidKeyException, NoSuchAlgorithmException, InvalidKeySpecException,
	 * InvalidAlgorithmParameterException, UnsupportedEncodingException,
	 * IllegalBlockSizeException, BadPaddingException { Session session =
	 * sessionFactory.openSession(); System.out.println("User Name Is::::::::::"
	 * + userName); System.out.println("Password Is:::::::::::" + password);
	 * List<LoginBean> loginValidation =null; try {
	 * System.out.println("Password :::"+password);
	 * PasswordEncryption.encrypt(password);
	 * 
	 * String encryptedPwd = PasswordEncryption.encStr;
	 * System.out.println("encripted Passw ::::::"+encryptedPwd); //String query
	 * =
	 * "SELECT * FROM sabpaisaportal.login_master where userName=aaaa and password=aaaaa "
	 * ; String status="approved"; String hql =
	 * "SELECT * FROM sabpaisaportal.login_master where userName= :username and password= :password and status= :status"
	 * ; SQLQuery sqlQuery = session.createSQLQuery(hql);
	 * sqlQuery.setParameter("username", userName);
	 * sqlQuery.setParameter("password", encryptedPwd);
	 * sqlQuery.setParameter("status", status);
	 * sqlQuery.addEntity(LoginBean.class); loginValidation = sqlQuery.list();
	 * 
	 * loginValidation = session.createCriteria(LoginBean.class)
	 * .add(Restrictions.eq("userName", userName.trim()))
	 * .add(Restrictions.eq("password", password.trim())).list();
	 * System.out.println("List Size ::::"+loginValidation.size()); return
	 * loginValidation; productBeanList = (List<OrganizationProductBean>)
	 * criteria.list(); } catch (java.util.NoSuchElementException ex) {
	 * ex.printStackTrace(); return null; } finally { session.close(); }
	 * //System.out.println(loginValidation.getPassword() + " : " +
	 * loginValidation.getUserName());
	 * 
	 * }
	 */

	public LoginBean validateLoginCredentialsDao(String userName, String password) {
		Session session = sessionFactory.openSession();
		System.out.println("User Name Is::::::::::" + userName);
		System.out.println("Password Is:::::::::::" + password);
		LoginBean loginBeanRecord = new LoginBean();
		try {

			Criteria cr = session.createCriteria(LoginBean.class);

			Criterion uname = Restrictions.eq("userName", userName);
			Criterion psw = Restrictions.eq("password",password);


			// To get records matching with AND conditions
			LogicalExpression andExp = Restrictions.and(uname, psw);
			cr.add(andExp);
System.out.println("size of list is = "+cr.list().size());
			loginBeanRecord= (LoginBean) cr.list().get(0);
			
			/*String hql = "SELECT * FROM sabpaisaportal.login_master where userName= :username and password= :password";
			SQLQuery sqlQuery = session.createSQLQuery(hql);
			sqlQuery.setParameter("username", userName);
			sqlQuery.setParameter("password", password);
			//sqlQuery.addSynchronizedEntityClass(LoginBean.class);
			loginBeanRecord=(LoginBean) sqlQuery.list();*/
			
			System.out.println("Size ::" + loginBeanRecord);
		} catch (java.util.NoSuchElementException ex) {
			ex.printStackTrace();
			return loginBeanRecord;
		} finally {
			session.close();
		}
		System.out.println(loginBeanRecord.getPassword() + " : " + loginBeanRecord.getUserName());
		return loginBeanRecord;
	}

}
